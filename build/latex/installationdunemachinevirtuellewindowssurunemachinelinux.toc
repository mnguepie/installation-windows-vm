\babel@toc {french}{}\relax 
\contentsline {chapter}{\numberline {1}Installation d’une Machine Virtuelle Windows sur une Machine Linux}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Introduction}{3}{chapter.2}%
\contentsline {chapter}{\numberline {3}Prérequis}{5}{chapter.3}%
\contentsline {chapter}{\numberline {4}Étape 1 : Installer VirtualBox}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}Sur Ubuntu/Debian}{7}{section.4.1}%
\contentsline {section}{\numberline {4.2}Sur Fedora}{7}{section.4.2}%
\contentsline {section}{\numberline {4.3}Sur Arch Linux}{7}{section.4.3}%
\contentsline {chapter}{\numberline {5}Étape 2 : Téléchargez le Fichier ISO de Windows}{9}{chapter.5}%
\contentsline {chapter}{\numberline {6}Étape 3 : Créer une Nouvelle Machine Virtuelle}{11}{chapter.6}%
\contentsline {section}{\numberline {6.1}Configuration de la Mémoire}{11}{section.6.1}%
\contentsline {section}{\numberline {6.2}Configuration du Disque Dur Virtuel}{11}{section.6.2}%
\contentsline {chapter}{\numberline {7}Étape 4 : Configuration de la Machine Virtuelle}{13}{chapter.7}%
\contentsline {chapter}{\numberline {8}Étape 5 : Démarrer l’Installation de Windows}{15}{chapter.8}%
\contentsline {chapter}{\numberline {9}Étape 6 : Installer les Additions Invité de VirtualBox (Guest Additions)}{17}{chapter.9}%
\contentsline {chapter}{\numberline {10}Conclusion}{19}{chapter.10}%
\contentsline {chapter}{\numberline {11}Indices et tables}{21}{chapter.11}%
