.. Installation d'une machine virtuelle Windows sur une machine Linux documentation master file, created by
   sphinx-quickstart on Tue Jul 23 13:15:45 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation de l'installation d'une machine virtuelle Windows sur une machine Linux
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Table des matières

   installation_windows_vm

Indices et tables
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
