Installation d'une Machine Virtuelle Windows sur une Machine Linux
===================================================================

Introduction
============

Cette documentation explique comment installer et configurer une machine virtuelle Windows sur une machine Linux en utilisant VirtualBox, un logiciel de virtualisation gratuit et open source.

Prérequis
=========

- Une machine Linux fonctionnelle
- Connexion Internet
- Un fichier ISO d'installation de Windows (téléchargeable depuis le site officiel de Microsoft)
- Accès administrateur pour installer des logiciels

Étape 1 : Installer VirtualBox
===============================

Sur Ubuntu/Debian
-----------------

1. Ouvrez un terminal.
2. Mettez à jour la liste des paquets :

   .. code-block:: bash

       sudo apt update

3. Installez VirtualBox :

   .. code-block:: bash

       sudo apt install virtualbox

Sur Fedora
----------

1. Ouvrez un terminal.
2. Installez VirtualBox :

   .. code-block:: bash

       sudo dnf install VirtualBox

Sur Arch Linux
--------------

1. Ouvrez un terminal.
2. Installez VirtualBox :

   .. code-block:: bash

       sudo pacman -S virtualbox

Étape 2 : Téléchargez le Fichier ISO de Windows
===============================================

1. Allez sur le site officiel de Microsoft : `Télécharger Windows 10 <https://www.microsoft.com/fr-fr/software-download/windows10ISO>`_
2. Téléchargez le fichier ISO correspondant à votre version de Windows.

Étape 3 : Créer une Nouvelle Machine Virtuelle
==============================================

1. Ouvrez VirtualBox depuis votre menu d'applications.
2. Cliquez sur "Nouvelle" pour créer une nouvelle machine virtuelle.
3. Entrez un nom pour la machine virtuelle, choisissez "Microsoft Windows" comme type et sélectionnez la version correspondante (par exemple, "Windows 10 (64-bit)").
4. Cliquez sur "Suivant".

Configuration de la Mémoire
---------------------------

1. Allouez de la mémoire RAM à la machine virtuelle. Il est recommandé d'allouer au moins 2 Go (2048 Mo), en fonction de la mémoire disponible sur votre machine hôte.
2. Cliquez sur "Suivant".

Configuration du Disque Dur Virtuel
-----------------------------------

1. Sélectionnez "Créer un disque dur virtuel maintenant" et cliquez sur "Créer".
2. Choisissez le format de fichier du disque dur virtuel. VDI (VirtualBox Disk Image) est recommandé.
3. Sélectionnez "Dynamiquement alloué" pour une utilisation efficace de l'espace disque.
4. Définissez la taille du disque dur virtuel. Il est recommandé de réserver au moins 20 Go.
5. Cliquez sur "Créer".

Étape 4 : Configuration de la Machine Virtuelle
===============================================

1. Sélectionnez la machine virtuelle que vous venez de créer et cliquez sur "Configuration".
2. Allez dans l'onglet "Stockage".
3. Sous "Contrôleur : SATA", cliquez sur l'icône du disque avec un signe plus pour ajouter un périphérique optique.
4. Sélectionnez "Ajouter une image de disque" et naviguez jusqu'à votre fichier ISO de Windows.
5. Cliquez sur "Choisir".
6. Cliquez sur "OK".

Étape 5 : Démarrer l'Installation de Windows
============================================

1. Sélectionnez la machine virtuelle et cliquez sur "Démarrer".
2. La machine virtuelle démarrera à partir de l'ISO de Windows.
3. Suivez les instructions à l'écran pour installer Windows.


Conclusion
==========

Vous avez maintenant une machine virtuelle Windows fonctionnant sur votre machine Linux. Vous pouvez utiliser cette machine virtuelle pour exécuter des applications Windows, tester des configurations, ou tout autre besoin.

Si vous rencontrez des problèmes ou avez des questions, consultez la documentation officielle de VirtualBox ou les forums de support pour obtenir de l'aide.
